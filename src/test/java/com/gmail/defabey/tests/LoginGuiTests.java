﻿package com.gmail.defabey.tests;

import com.gmail.defabey.Constants;
import com.gmail.defabey.pages.LoginPageObject;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.core.Is.is;

/**
 * Created by defabey on 07-Jun-18.
 */
public class LoginGuiTests extends GuiTests {

    private LoginPageObject page;

    @Override
    @Before
    public void setUp() {
        super.setUp();
        page = new LoginPageObject(getDriver());
    }

    @Override
    @After
    public void tearDown() {
        super.tearDown();
    }

    private void loginUsingCorrectCredentials() {
        page.login(Constants.CORRECT_EMAIL, Constants.CORRECT_PASS);
    }

    @Test
    public void positiveLogin() {
        loginUsingCorrectCredentials();
        Assert.assertThat("Auth errors should not be displayed if login/pass are correct",
                page.authAlertsHolder.isDisplayed(),
                is(false));
        Assert.assertThat("Auth pane should not be displayed if login is successful",
                page.authPage.isDisplayed(),
                is(false));
    }

    @Test
    public void emptyEmailAndPasswordLogin() {
        page.login("", "");
        Assert.assertThat("Error text differs from expected",
                page.authAlertsHolder.getText(),
                is(Constants.WRONG_EMAIL_FORMAT));
    }


    @Test
    public void wrongEmailFormatAndEmptyPasswordLogin() {
        page.login("a", "");
        Assert.assertThat("Error text differs from expected",
                page.authAlertsHolder.getText(),
                is(Constants.WRONG_EMAIL_FORMAT));
    }

    @Test
    public void correctEmailAndWrongPasswordLogin() {
        page.login(Constants.CORRECT_EMAIL, "ololo");
        Assert.assertThat("Error text differs from expected",
                page.authAlertsHolder.getText(),
                is(Constants.WRONG_EMAIL_OR_PASS));
    }
}
