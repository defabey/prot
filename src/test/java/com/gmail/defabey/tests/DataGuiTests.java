package com.gmail.defabey.tests;

import com.gmail.defabey.Constants;
import com.gmail.defabey.pages.InputPageObject;
import com.gmail.defabey.pages.LoginPageObject;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.hamcrest.core.Is.is;

/**
 * Created by defabey on 07-Jun-18.
 */
public class DataGuiTests extends GuiTests {

    private InputPageObject page;

    @Override
    @Before
    public void setUp() {
        super.setUp();
        new LoginPageObject(getDriver()).login(Constants.CORRECT_EMAIL, Constants.CORRECT_PASS);
        page = new InputPageObject(getDriver());
    }

    @Override
    @After
    public void tearDown() {
        super.tearDown();
    }

    @Test
    public void addEmptyRecord() {
        page.dataSend.click();
        Assert.assertThat(page.dataAlertsHolder.getText(),
                is(Constants.WRONG_EMAIL_FORMAT));
    }

    @Test
    public void addRecordOnlyWithEmail() {
        page.dataEmail.sendKeys(Constants.CORRECT_EMAIL);
        page.dataSend.click();
        Assert.assertThat(page.dataAlertsHolder.getText(),
                is(Constants.NAME_FIELD_CAN_NOT_BE_EMPTY));
    }

    @Test
    public void addRecordOnlyWithEmailAndName() {
        page.dataEmail.sendKeys(Constants.CORRECT_EMAIL);
        String name = "fedot";
        page.dataName.sendKeys(name);
        page.dataSend.click();

        checkAddedModalAndClose();
        List<WebElement> tableRowList = page.dataTable.findElements(By.cssSelector("tbody tr"));
        Assert.assertThat("only one row is required",
                tableRowList.size(),
                is(1));
        List<String> rowElems = tableRowList.get(0).findElements(By.tagName("td")).stream()
                .map(WebElement::getText)
                .collect(Collectors.toList());
        List<String> expected = Arrays.asList(Constants.CORRECT_EMAIL, name, Constants.MALE, "Нет", "");
        Assert.assertThat("added row differs from expected",
                rowElems,
                is(expected));
    }

    private void checkAddedModalAndClose() {
        WebElement modal = getDriver().findElement(By.cssSelector(Constants.MODAL_CSS_SELECTOR));
        Assert.assertThat(modal.findElement(By.cssSelector(".uk-modal-content")).getText(),
                is(Constants.DATA_IS_ADDED));
        modal.findElement(By.cssSelector("button.uk-modal-close")).click();
        sleepForSecond();
    }
}
