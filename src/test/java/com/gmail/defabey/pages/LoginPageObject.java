package com.gmail.defabey.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.concurrent.TimeUnit;

/**
 * Created by defabey on 07-Jun-18.
 */
public class LoginPageObject {
    private WebDriver driver;

    @FindBy(css = "#authPage")
    public WebElement authPage;
    @FindBy(css = "#authPage #authAlertsHolder")
    public WebElement authAlertsHolder;
    @FindBy(css = "#authPage #loginEmail")
    public WebElement loginEmail;
    @FindBy(css = "#authPage #loginPassword")
    public WebElement loginPassword;
    @FindBy(css = "#authPage #authButton")
    public WebElement authButton;

    public LoginPageObject(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void login(String email, String pass) {
        loginEmail.sendKeys(email);
        loginPassword.sendKeys(pass);
        authButton.click();
        sleepForSecond();
    }

    private void sleepForSecond() {
        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException ignored) {
        }
    }
}
