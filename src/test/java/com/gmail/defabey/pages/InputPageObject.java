package com.gmail.defabey.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by defabey on 07-Jun-18.
 */
public class InputPageObject {
    private WebDriver driver;

    @FindBy(css = "#inputsPage")
    public WebElement inputsPage;
    @FindBy(css = "#inputsPage #dataAlertsHolder")
    public WebElement dataAlertsHolder;
    @FindBy(css = "#inputsPage #dataEmail")
    public WebElement dataEmail;
    @FindBy(css = "#inputsPage #dataName")
    public WebElement dataName;
    //Select
    @FindBy(css = "#inputsPage #dataGender")
    public WebElement dataGender;
    @FindBy(css = "#inputsPage #dataCheck11")
    public WebElement dataCheck11;
    @FindBy(css = "#inputsPage #dataCheck12")
    public WebElement dataCheck12;
    @FindBy(css = "#inputsPage #dataSelect21")
    public WebElement dataSelect21;
    @FindBy(css = "#inputsPage #dataSelect22")
    public WebElement dataSelect22;
    @FindBy(css = "#inputsPage #dataSelect23")
    public WebElement dataSelect23;

    @FindBy(css = "#inputsPage #dataSend")
    public WebElement dataSend;

    @FindBy(css = "#inputsPage #dataTable")
    public WebElement dataTable;

    public InputPageObject(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }
}
