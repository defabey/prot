﻿package com.gmail.defabey;

/**
 * Created by defabey on 07-Jun-18.
 */
public class Constants {
    public static final String PATH = "C:\\Users\\defabey\\Downloads\\qa-test.html";

    public static final String CORRECT_EMAIL = "test@protei.ru";
    public static final String CORRECT_PASS = "test";

    public static final String WRONG_EMAIL_FORMAT = "Неверный формат E-Mail";
    public static final String WRONG_EMAIL_OR_PASS = "Неверный E-Mail или пароль";
    public static final String NAME_FIELD_CAN_NOT_BE_EMPTY = "Поле имя не может быть пустым";
    public static final String DATA_IS_ADDED = "Данные добавлены.";
    public static final String MALE = "Мужской";
    public static final String FEMALE = "Женский";

    public static final String MODAL_CSS_SELECTOR = ":root .uk-modal .uk-modal-dialog";
}
